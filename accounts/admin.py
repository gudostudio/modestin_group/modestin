from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin


class UserAccountAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (
            ('Доп. поля', {'fields': tuple([field.name for field in UserAccount._meta.get_fields()][12:-2])}),
    )


admin.site.register(UserAccount, UserAccountAdmin)