from django.apps import AppConfig


class AdminHostsConfig(AppConfig):
    name = 'admin_host'
