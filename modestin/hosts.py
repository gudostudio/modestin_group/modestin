# _*_ coding:utf-8 _*_
from django.conf import settings
from django_hosts import patterns, host


# url по поддоменам
host_patterns = patterns('',
    host(r'admin', 'admin_host.urls', name='admin_host'),  # если admin, то на urlы приложения admin_host
    host(r'www', 'modestin.urls', name='www_host'),  # если www
    host(r'', 'modestin.urls', name='main_host'),  # или пустой, то как обычно, посылаем на самый главный url
)
