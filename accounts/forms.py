from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
from . import validators
from django.contrib.auth.validators import ASCIIUsernameValidator
from django.core.exceptions import ValidationError
User = get_user_model()


class UserRegistrationForm(UserCreationForm):
    username = forms.CharField(max_length=50, label='Логин*', validators=[ASCIIUsernameValidator()])
    password1 = forms.CharField(max_length=50, widget=forms.PasswordInput, label="Пароль*", validators=[validators.validate_password])
    password2 = forms.CharField(max_length=50, widget=forms.PasswordInput, label="Подтверждение пароля*", validators=[validators.validate_password])

    class Meta:
        model = User
        error_css_class = 'error'
        fields = ('username', 'password1', 'password2')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'


class LoginForm(forms.Form):
    login = forms.CharField(max_length=60, label='Имя пользователя')
    password = forms.CharField(max_length=50, widget=forms.PasswordInput, label='Пароль')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control mb-3'
        self.user_cache = None

    def clean(self):
        password = self.cleaned_data.get('password', None)
        login = self.cleaned_data.get('login', None)
        try:
            user = User.objects.get(username=login)
        except User.DoesNotExist:
            try:
                user = User.objects.get(email=login)
            except User.DoesNotExist:
                user = None
        if user is None:
            raise ValidationError('Неверный логин или пароль')

        if not user.check_password(password):
            raise ValidationError('Неверный логин или пароль')

        self.user_cache = user

        return self.cleaned_data


class SecondUserRegistrationForm(forms.ModelForm):
    fio = forms.CharField(label='ФИО', max_length=200, validators=[validators.validate_fio])

    class Meta:
        model = User
        error_css_class = 'error'
        fields = (
            'activity',
            'company',
            'postal_address',
            'email',
            'tel_number',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
