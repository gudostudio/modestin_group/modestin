# _*_ coding:utf-8 _*_
from .settings import *


# SECURITY
SECRET_KEY = '000000000000000000000000000000000000000000000000000'

# debug mod
DEBUG = True

ALLOWED_HOSTS = ['*']

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'dev.db'),
    }
}


# static
STATIC_ROOT = None
STATICFILES_DIRS = (
  os.path.join(BASE_DIR, 'static'),
)

SITE_URL = 'localtest.me:8000/'

SESSION_COOKIE_DOMAIN = '.localtest.me'

# MAIL
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 587
EMAIL_HOST_USER = "develop@gudostudio.ru"
EMAIL_HOST_PASSWORD = "12345qwert"
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'ModestinDEV <develop@gudostudio.ru>'
SERVER_EMAIL = 'develop@gudostudio.ru'
