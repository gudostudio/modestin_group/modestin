from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser



def generate_url(instance, filename):
    """Generate url for user avatar"""
    file_type = filename.split('.')[-1]  # take file type (png, jpg, ..)
    last_id = UserAccount.objects.all().order_by('id')  # find last user
    if last_id:  # if exists
        last_id = last_id[0].id  # take his id
    if not last_id:  # else
        new_id = 1  # will be 1
    else:  # if exists
        new_id = last_id + 1  # plus 1
    filename = 'image-' + str(new_id) + '.' + file_type  # compare all
    return "avatars/" + filename  # returning url


class UserAccount(AbstractUser):
    """User account. Added fields"""
    patronymic = models.CharField(verbose_name='Отчество', max_length=128, blank=True)
    activity = models.CharField(verbose_name='Деятельность', max_length=128, blank=True)
    company = models.CharField(verbose_name='Название компании', max_length=255, blank=True)
    postal_address = models.CharField(verbose_name='Почтовый адрес', max_length=255, blank=True)
    image = models.ImageField(upload_to=generate_url, default='default/user.png', blank=False, null=False, verbose_name="Аватарка")
    tel_number = models.CharField(verbose_name='Номер телефона', max_length=255, blank=True)
    position = models.CharField(verbose_name='Должность', max_length=128, blank=True)

    def __str__(self):
        return str(self.username)

    class Meta:
        verbose_name = 'Аккаунт пользователя'
        verbose_name_plural = 'Аккаунты пользователей'
